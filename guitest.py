from array import array
from math import sin, pi
from random import random
import matplotlib.pyplot as plt
import glfw
import OpenGL.GL as gl
import imgui
from imgui.integrations.glfw import GlfwRenderer
from time import time
import pyrealsense2 as rs
import cv2
import imgui_cv
import imgui_fig
import numpy as np
from mpl_toolkits import mplot3d

C = .01
L = int(pi * 2 * 100)
fix_ridge = None
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

pipeline.start(config)
font = cv2.FONT_HERSHEY_SIMPLEX

def empDepth(_img):
    print(_img[240][320])


def show_fps():
    imgui.set_next_window_position(0, 20, imgui.APPEARING)
    imgui.set_next_window_size(100, 40, imgui.APPEARING)
    imgui.begin("FPS")
    msg = "{0:.1f}".format(60)
    imgui.text(msg)
    imgui.end()

def show_fps2():
    imgui.set_next_window_position(700, 700, imgui.APPEARING)
    imgui.set_next_window_size(100, 40, imgui.APPEARING)
    imgui.begin("FPS")
    msg = "{0:.1f}".format(60)
    imgui.text(msg)
    imgui.end()
def show_ridge():
    
    imgui.set_next_window_position(0, 560, imgui.APPEARING)
    imgui.set_next_window_size(640, 140, imgui.APPEARING)
    imgui.begin("ridge")
    imgui.plot_histogram(
            "",
            fix_ridge,
            #overlay_text="random histogram",
            # offset by one item every milisecond, plot values
            # buffer its end wraps around
            graph_size=(640, 100),
        )
    imgui.end()
 
def show_arm(_depth):
    imgui.set_next_window_position(0, 560, imgui.APPEARING)
    imgui.set_next_window_size(640, 480, imgui.APPEARING)
    imgui.begin("ARM")
    figure = plt.figure() #figsize(10,10)
    ax = plt.axes(projection='3d')
    x = np.linspace(0,639,640)
    y = np.linspace(0,479,480)
    #print(_depth.shape)
    X,Y=np.meshgrid(x,y)
    #z = np.array([[1 for v in range(640)] for u in range(480)])
    #print(z.shape)
    ax.contour3D(X,Y,_depth,50,cmap='binary')
    ax.view_init(60,35)
    imgui_fig.fig(figure,height = 400,title = "")

    plt.clf()
    plt.close()

    imgui.end()
    pass

def show_fig(_contour):
    imgui.set_next_window_position(1300, 20, imgui.APPEARING)
    imgui.set_next_window_size(360, 300, imgui.APPEARING)
    imgui.begin("contour")
    figure = plt.figure()
    x = np.arange(0, 320, 1)
    y = _contour
    plt.plot(x, y)

    imgui_fig.fig(figure, height=250, title="")
    #plt.cla()
    plt.clf()
    plt.close()
    imgui.end()

def show_depth(_depth,_ridge,_sx,_ex):
    global fix_ridge

    imgui.set_next_window_position(650, 40, imgui.APPEARING)
    imgui.set_next_window_size(640, 640, imgui.APPEARING)
    imgui.begin("DEPTH")
    #dist2 = cv2.normalize(_depth, None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8UC3)
    depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(_depth, alpha=0.03), cv2.COLORMAP_JET)
    cv2.line(depth_colormap, (320, 80), (320, 400), (0, 0, 255), 3)
    cv2.line(depth_colormap, (_sx, 240), (_ex, 240), (0, 255, 0), 2)

    imgui_cv.image(depth_colormap, height=480, title="")  # returns mouse_position
    
    

    #histogram_values = array('f', [random() for x in range(20)])
    if imgui.button("check"):
        fix_ridge = array('f', _ridge)
        print("@@")
    histogram_values = array('f', _ridge)
    imgui.plot_histogram(
            "",
            #"histogram(random())",
            histogram_values,
            #overlay_text="random histogram",
            # offset by one item every milisecond, plot values
            # buffer its end wraps around
            graph_size=(640, 100),
        )
    

    imgui.end()



def plot_depth():

    imgui.set_next_window_position(0, 400, imgui.APPEARING)
    imgui.set_next_window_size(320, 320, imgui.APPEARING)
    imgui.begin("hand")
    histogram_values = array('f', [random() for _ in range(20)])
    imgui.plot_histogram(
            "histogram(random())",
            histogram_values,
            overlay_text="random histogram",
            # offset by one item every milisecond, plot values
            # buffer its end wraps around
            graph_size=(0, 50),
        )
    imgui.end()

def main():
    global fix_ridge
    window = impl_glfw_init()
    imgui.create_context()
    impl = GlfwRenderer(window)

    plot_values = array('f', [sin(x * C) for x in range(L)])
    histogram_values = array('f', [random() for _ in range(20)])

    while not glfw.window_should_close(window):
        glfw.poll_events()
        impl.process_inputs()

        imgui.new_frame()


        if imgui.begin_main_menu_bar():
            if imgui.begin_menu("File", True):

                clicked_quit, selected_quit = imgui.menu_item(
                    "Quit", 'Cmd+Q', False, True
                )

                if clicked_quit:
                    exit(1)

                imgui.end_menu()
            imgui.end_main_menu_bar()


        

        show_fps()
        
        #show_fig()
        #plot_depth()

        if fix_ridge != None:
            show_ridge()


        frames = pipeline.wait_for_frames()

        depth_frame = frames.get_depth_frame()
      
        color_frame = frames.get_color_frame()

        if not depth_frame or not color_frame:
            pass
        else:
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())
            #print(color_image.shape)
            contour = [0]*320
            dcontour = [0]*320
            i = 0
            for x in range(320):
                contour[i] = depth_frame.get_distance(x+160, 240)

                i+=1
            i = 0
            for x in range(318):
                dcontour[i] = abs(contour[x+1] - contour[x]) 
                if dcontour[i] > 0.1:
                    dcontour[i] = 0.1
                i+=1
            ex = 159
            sx = 158
            for x in range(158,0,-1):
               if dcontour[x] > dcontour[sx]:
                   sx = x
            for x in range(159):
               if dcontour[x+159] > dcontour[ex]:
                   ex = x+159

            coverage = [0]*320
            i = 0
            for x in range(320):
                coverage[i] = depth_frame.get_distance(320, 80+x)
                i+=1


            show_depth(depth_image,coverage,sx+160,ex+160)

            #print(depth_image.shape)
            show_arm(depth_image)


            show_fig(contour)


            #print(depth_image.shape)
            #print("@",depth_frame.get_distance(320, 240))
            _tmpimg = np.zeros((480, 640))
            # for y in range(480):
            #     for x in range(640):
            #         _tmpimg[y][x] = depth_frame.get_distance(x, y)*1000
                    # if _tmpimg[y][x]>255:
                    #     _tmpimg[y][x] = 255
                    # if _tmpimg[y][x]<100:
                    #     _tmpimg[y][x] = 100
            #dist1 = cv2.convertScaleAbs(_tmpimg)
            dist2 = cv2.normalize(_tmpimg, None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8UC1)
            im_color = cv2.applyColorMap(dist2, cv2.COLORMAP_JET)
            cv2.line(color_image, (380, 80), (380, 400), (0, 0, 255), 3)
            cv2.line(color_image, (sx+220, 240), (ex+220, 240), (255, 0, 0), 3)
            z = np.copy(color_image)
            #dist2 = cv2.normalize(_depth, None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8UC3)
            #dist3 = cv2.normalize(_tmpimg, None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8UC1)

            #empDepth(depth_image)
        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            #depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(_tmpimg, alpha=0.03), cv2.COLORMAP_JET)#COLORMAP_JET BONE 0.03
        # Stack both images horizontally
            #images = np.hstack((color_image)) #depth_colormap
            #cv2.line(images, (320, 80), (320, 400), (0, 0, 255), 3)
            #imgui.plot(images)
            #imgui.image(images, height=500,title="flowers")
            # ImGuiImageLister.push_image("owl", images)
            # ImGuiLister_ShowStandalone()
            # draw_list = imgui.get_window_draw_list()
            # draw_list.add_image()
            #imgui.new_frame()
            imgui.set_next_window_position(0, 40, imgui.APPEARING)
            imgui.set_next_window_size(640, 480, imgui.APPEARING)
            imgui.begin("RGB CAMERA")

            imgui_cv.image(z, height=480, title="")  # returns mouse_position
            # imgui.plot_lines(
            #     "Sin(t)",
            #     plot_values,
            #     overlay_text="SIN() over time",
            # # offset by one item every milisecond, plot values
            # # buffer its end wraps around
            #     values_offset=int(time() * 100) % L,
            # # 0=autoscale => (0, 50) = (autoscale width, 50px height)
            #     graph_size=(0, 50),
            # )
            imgui.end()
            #imgui.push_image("dark",images)


            
            #imgui.end()

        

            # imgui.render()
            # imgui.new_frame()

            # imgui.begin("RGB CAMERA")
        # imgui.plot_histogram(
        #     "histogram(random())",
        #     histogram_values,
        #     overlay_text="random histogram",
        #     # offset by one item every milisecond, plot values
        #     # buffer its end wraps around
        #     graph_size=(0, 50),
        # )





            

        gl.glClearColor(0.1, 0.1, 0.1, 0.1)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)

        imgui.render()
        impl.render(imgui.get_draw_data())
        glfw.swap_buffers(window)

    impl.shutdown()
    glfw.terminate()


def impl_glfw_init():
    width, height = 1680, 720
    window_name = "Ultrasound 3D"

    if not glfw.init():
        print("Could not initialize OpenGL context")
        exit(1)

    # OS X supports only forward-compatible core profiles from 3.2
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
    #glfw.window_hint()
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, gl.GL_TRUE)

    # Create a windowed mode window and its OpenGL context
    window = glfw.create_window(
        int(width), int(height), window_name, None, None
    )
    glfw.make_context_current(window)

    if not window:
        glfw.terminate()
        print("Could not initialize Window")
        exit(1)

    return window


if __name__ == "__main__":
    main()