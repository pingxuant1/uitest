import matplotlib.pyplot as plt
from array import array
from math import sin, pi
from random import random
import numpy as np
from time import time
import pyrealsense2 as rs
import cv2
import dlib
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
import imutils
# alignmentDepthandColor
from pyzbar import pyzbar
import process_func as pf

def click_and_crop(event, x, y, flags, param):
    pass

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
#config.enable_stream(rs.stream.infrared,1, 640, 640, rs.format.y8, 30)
#config.enable_stream(rs.stream.infrared)

# Dlib 的人臉偵測器
detector = dlib.get_frontal_face_detector()



pipeline.start(config)

for x in range(5):
    pipeline.wait_for_frames()


align = rs.align(rs.stream.color)
#frameset = align.process(frameset)


widthCM = 10 
heightCM = 10
# fig = plt.figure()
# ax = Axes3D(fig)
# #ax.hold(True)

# plt.show(False)
# plt.draw()
dst_pts = None
dst= None

try:
    frames = pipeline.wait_for_frames()
    frames = align.process(frames)
    depth_frame = frames.get_depth_frame()
        #depth_frame = frames.get_infrared_frame(1)
    color_frame = frames.get_color_frame()
    color_image = np.asanyarray(color_frame.get_data())

    

    roi=cv2.selectROI(windowName="roi",img=color_image,showCrosshair=True,fromCenter=False)
    cv2.destroyWindow("roi")
    x,y,w,h=roi
    widthCM = w
    heightCM = h

    # roi=cv2.selectROI(windowName="roi",img=color_image,showCrosshair=True,fromCenter=False)
    # cv2.destroyWindow("roi")
    # x,y,w,h=roi
    # img_org = color_image[y:y+h,x:x+w]
    # scale_factor = 0.8
    # # img1 = pf.image_proc(cv2.resize(img_org,None,fx=scale_factor,fy=scale_factor),scale_factor)
    # img1 = pf.image_proc(img_org,scale_factor)
    # img2_fframe = pf.image_proc(color_image, 0.5)
    # dst_pts, dst = pf.brisk_flann(img1, img2_fframe)
    # src_pts = np.copy(dst_pts)
    # roi_image = color_image[y:y+h,x:x+w]
    
    # nums = [0, 0, 0]
    # # rec = 20
    # nums[0] = round(np.sum(roi_image[:,:,0]) / w*h)
    # nums[1] = round(np.sum(roi_image[:,:,1]) / w*h)
    # nums[2] = round(np.sum(roi_image[:,:,2]) / w*h)

    while True:
 
        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        frames = align.process(frames)
        #depth_frame = frames.first(rs.stream.infrared)
        depth_frame = frames.get_depth_frame()
        #depth_frame = frames.get_infrared_frame(1)
        color_frame = frames.get_color_frame()
        if not depth_frame or not color_frame:
            continue
        


        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        #print(depth_image.shape,depth_image[0][0])
        color_image = np.asanyarray(color_frame.get_data())
        #depth_image = cv2.cvtColor(depth_image, cv2.COLOR_GRAY2BGR)

        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)


        # img2_fframe = pf.image_proc(color_image, 0.5)
        # dst_pts, dst = pf.brisk_flann(img1, img2_fframe)
        # img_marked = pf.draw_frame(color_image, dst)
        
        #face_rects = detector(color_image, 0)

        #barcodes = pyzbar.decode(color_image)
        img_gray = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
        img_gb = cv2.GaussianBlur(img_gray, (5, 5), 0)
        edges = cv2.Canny(img_gray, 150 , 300)

        contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #img_fc, 
        hierarchy = hierarchy[0]
        found = []
        for i in range(len(contours)):
           k = i
           c = 0
           while hierarchy[k][2] != -1:
               k = hierarchy[k][2]
               c = c + 1
           if c >= 5:
               found.append(i)
        qq = []
        mx = []
        my = []
        for i in found:
           #img_dc = img.copy()
           M = cv2.moments(contours[i])
           cX = int(M["m10"] / M["m00"])
           cY = int(M["m01"] / M["m00"])
           qq.append([cv2.contourArea(contours[i]),cX,cY])
           #cv2.circle(color_image, (cX, cY), 7, (255, 255, 255), -1)
           cv2.drawContours(color_image, contours, i, (0, 255, 0), 3)

           zt = depth_frame.get_distance(int(cX), int(cY))
           xt = int(cX*10/widthCM)
           yt = int(cY*10/heightCM)
           text = f'x:{round(xt)} y:{round(yt)} z:{round(zt*100)}'
           cv2.putText(color_image, text, (int(cX), int(cY)), cv2.FONT_HERSHEY_COMPLEX_SMALL,1, (0, 255, 255), 1, cv2.LINE_AA)
           #if len(contours) > 2:
               
           #print(i,len(contours[i]))
           #break
        qq.sort(key=lambda k: [k[0]])

        #print(qq)
        if len(qq) ==3 or len(qq) ==4 and False:
            #print(qq[0][1])
            cv2.arrowedLine(color_image, (int(qq[2][1]),int(qq[2][2])),  (int(qq[0][1]),int(qq[0][2])), (0,0,255), 5)
            zt = depth_frame.get_distance(int(qq[2][1]), int(qq[2][2])) #*640/1280 + 50 *480/720 + 50
            cv2.circle(color_image,(int(qq[2][1]),int(qq[2][2])), 10, (0, 0, 255), -1)

            xt = int(qq[2][1]*10/widthCM)
            yt = int(qq[2][2]*10/heightCM)
            text = f'x:{round(xt)} y:{round(yt)} z:{round(zt*100)} '  #x : {cs[0]}\n y: {cs[1]}\n
            #{int(qq[2][1]*640/1280)} {int(qq[2][2]*480/720)}
            cv2.putText(color_image, text, (int(qq[2][1]), int(qq[2][2])), cv2.FONT_HERSHEY_COMPLEX_SMALL,1, (0, 255, 255), 1, cv2.LINE_AA)

        #for i, d in enumerate(face_rects):
        # for barcode in barcodes:
        # #     # x1 = d.left()
        # #     # y1 = d.top()
        # #     # x2 = d.right()
        # #     # y2 = d.bottom()

        #     (x1, y1, w, h) = barcode.rect
        #     x2 = x1 + w
        #     y2 = y1 + h
        #     # 以方框標示偵測的人臉
        #     cv2.rectangle(color_image, (x1, y1), (x2, y2), (0, 255, 0), 4, cv2.LINE_AA)

        #     #get depth
        #     cx = (x1 + x2)/2
        #     cy = (y1 + y2)/2
        #     zt = depth_frame.get_distance(int(cx), int(cy))
        #     text = f'x:{round(cx/widthCM)} y:{round(cy/heightCM)} z:{round(zt*100)}'  #x : {cs[0]}\n y: {cs[1]}\n 
        #     cv2.putText(color_image, text, (int(x1), int(y1)), cv2.FONT_HERSHEY_COMPLEX_SMALL,1, (0, 255, 255), 1, cv2.LINE_AA)
 
        # nums = [0, 0, 0]
        # rec = 20
        # nums[0] = round(np.sum(color_image[0:rec,0:rec,0]) / rec**2)
        # nums[1] = round(np.sum(color_image[0:rec,0:rec,1]) / rec**2)
        # nums[2] = round(np.sum(color_image[0:rec,0:rec,2]) / rec**2)



        #print(color_image[0][0],color_image.shape)

        #cv2.rectangle(color_image, (0, 0), (rec,rec), (0, 255, 0), 1)


        # text = f'B : {nums[0] } G:{nums[1]} R:{nums[2] }'
        # tmp = nums[0] + nums[1] +nums[2]
        # text1 = f'B/R : {round(nums[0]/nums[2],3) } G/R:{round(nums[1]/nums[2],3)} '
        # text2 = f'Br : {round(nums[0]/tmp,3) } Gr:{round(nums[1]/tmp,3)} Rr:{round(nums[2]/tmp,3)}'
        # text3 = f'B/G : {round(nums[0]/nums[1],3) } R/G:{round(nums[2]/nums[1],3)} '

        # r1 = 0.25#round(nums[0]/nums[2],3)
        # r2 = 0.25#round(nums[1]/nums[2],3)
        # r3 = 0.99
        # r4 = 0.1
        # cv2.putText(color_image, text, (0, 200), cv2.FONT_HERSHEY_TRIPLEX,1, (0, 255, 255), 1, cv2.LINE_AA)
        # cv2.putText(color_image, text1, (0, 100), cv2.FONT_HERSHEY_TRIPLEX,1, (0, 255, 255), 1, cv2.LINE_AA)
        # cv2.putText(color_image, text2, (0, 150), cv2.FONT_HERSHEY_TRIPLEX,1, (0, 255, 255), 1, cv2.LINE_AA)
        # cv2.putText(color_image, text3, (0, 250), cv2.FONT_HERSHEY_TRIPLEX,1, (0, 255, 255), 1, cv2.LINE_AA)


        # tx = []
        # ty = []
        # for i in range(0,480,5):
        #     for j in range(0,640,5):
        #         if abs((color_image[i,j,0] - r1*color_image[i,j,2])) < 0.2*color_image[i,j,2]:
        #             if abs((color_image[i,j,1]/color_image[i,j,2]) - r2) <0.2 :
        #                 cv2.circle(color_image,(j, i), 1, (255, 255, 255), 1)
        #                 tx.append([j,i])
        #         if abs(color_image[i,j,0] - r3*color_image[i,j,1]) <0.2*color_image[i,j,1]:
        #             if abs((color_image[i,j,2]/color_image[i,j,1]) - r4) <0.2 :
        #                 cv2.circle(color_image,(j, i), 1, (255, 0, 255), 1)
        #                 tx.append([j,i])
        # if len(tx) > 3:
        #     kmeans = KMeans(n_clusters=2, random_state=0).fit( np.array(tx))
        #     cc = kmeans.cluster_centers_
        #     for cs in cc:
        #         count = 0
        #         for _t in tx:
        #             if (_t[0]-cs[0])**2 + (_t[1]-cs[1])**2 < 1600:
        #                 count+=1
        #         if count > 10:
        #             zt = depth_frame.get_distance(int(cs[0]), int(cs[1]))
        #             cv2.circle(color_image,(int(cs[0]),int(cs[1])), 10, (0, 0, 255), -1)
        #             text4 = f'z:{round(zt*100)} '  #x : {cs[0]}\n y: {cs[1]}\n 
        #             cv2.putText(color_image, text4, (int(cs[0]), int(cs[1])), cv2.FONT_HERSHEY_COMPLEX_SMALL,1, (0, 255, 255), 1, cv2.LINE_AA)
        #             #print(cs)
        # # Stack both images horizontally
        # images = np.hstack((color_image, depth_colormap))
        

        # Show images
        #cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', color_image)
 

       # cv2.imshow('rr',img_marked)
        # X, Y value
        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')

        # Generate the values
        # r, g, b = cv2.split(color_image)
        # r = r.flatten()
        # g = g.flatten()
        # b = b.flatten()

        # ax.scatter(r, g, b)
        # fig.canvas.draw()
        #plt.pause(0.05)
        # plt.show()
        #plt.show(False)
        #plt.draw()
        key = cv2.waitKey(1)
        # Press esc or 'q' to close the image window
        if key & 0xFF == ord('q') or key == 27:
            cv2.destroyAllWindows()
            break
 
 
finally:
 
    # Stop streaming
    pipeline.stop()
    #plt.close(fig)
    cv2.destroyAllWindows()
    
