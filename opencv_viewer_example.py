## License: Apache 2.0. See LICENSE file in root directory.
## Copyright(c) 2015-2017 Intel Corporation. All Rights Reserved.

###############################################
##      Open CV and Numpy integration        ##
###############################################

import pyrealsense2 as rs
import numpy as np
import cv2
from decimal import *
getcontext().prec = 3
# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipeline.start(config)
font = cv2.FONT_HERSHEY_SIMPLEX
depth_frame = None
mx = 0
my = 0
getcontext().prec = 3
def click_and_crop(event, x, y, flags, param):
    global depth_frame,mx,my
    if event == cv2.EVENT_LBUTTONDOWN:
        print(f'x: {x} y: {y}')
    if x>640:
        #dist = depth_frame.get_distance(x-640, y)
        #print(dist)

        mx = x
        my  = y
    pass
    



try:
    cv2.namedWindow("RealSense",cv2.WINDOW_AUTOSIZE)
    cv2.setMouseCallback("RealSense", click_and_crop)

    while True:

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()
        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())

        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

        # Stack both images horizontally
        images = np.hstack((color_image, depth_colormap))
        if True:
            coverage = [0]*640
            i = 0
            for x in range(0,640,4):
                coverage[i] = depth_frame.get_distance(x, 160)
                i+=1
            for x in range(1,159):
                if coverage[i] < 0.1:
                    coverage[i] = (coverage[i-1] + coverage[i+1])/2
            L = 80
            for x in range(80,120):
                if abs(coverage[x] - coverage[x-1]) > 0.02:
                    L = x
                    break
            R = 80
            for x in range(80,40,-1):
                if abs(coverage[x] - coverage[x-1]) > 0.02:
                    R = x
                    break
            #dist = depth_frame.get_distance(mx-640, my)
            if R < L:
                cv2.line(images, (R*4+640, 160), (L*4+640, 160), (0, 0, 255), 5)
            #cv2.putText(images, str(int(coverage[L]*1000)) + " mm", (L*4+640,160), font, 0.5, (0, 255, 0), 2, cv2.LINE_AA)
            #cv2.putText(images, str(int(coverage[R]*1000)) + " mm", (R*4+640,160), font, 0.5, (0, 255, 0), 2, cv2.LINE_AA)
        #print(mx,my)
        # Show images
        #cv2.namedWindow('RealSense',cv2.WINDOW_AUTOSIZE )
        cv2.imshow('RealSense', images)

        k = cv2.waitKey(1)

        if k == ord('q'):
            break


finally:

    # Stop streaming
    pipeline.stop()