import pyrealsense2 as rs
import cv2

import numpy as np


pipe = rs.pipeline()
profile = pipe.start()
try:
  for i in range(0, 100):
    frames = pipe.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    if not depth_frame: continue
    depth_data = depth_frame.as_frame().get_data()
    np_image = np.asanyarray(depth_data)
    # for f in frames:
    #   cv2.imshow("Window", f.)	
    #   print(f.profile)
    width = depth_frame.get_width()
    height = depth_frame.get_height()
    print(width,height)
    dist_to_center = depth_frame.get_distance(int(width/2), int(height/2))
    print('The camera is facing an object:',dist_to_center,'meters away')
    #print(type(depth_frame.get_depth_frame()))
    cv2.imshow("Window",np_image)
finally:
    pipe.stop()