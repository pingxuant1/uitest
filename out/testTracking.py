import matplotlib.pyplot as plt
from array import array
from math import sin, pi
from random import random
import numpy as np
from time import time
import pyrealsense2 as rs
import cv2
import dlib
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
import imutils
from pyzbar import pyzbar
import process_func as pf


pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)


pipeline.start(config)

for x in range(5):
    pipeline.wait_for_frames()


align = rs.align(rs.stream.color)


widthCM = 10 
heightCM = 10

dst_pts = None
dst= None

try:
    frames = pipeline.wait_for_frames()
    frames = align.process(frames)
    depth_frame = frames.get_depth_frame()
        #depth_frame = frames.get_infrared_frame(1)
    color_frame = frames.get_color_frame()
    color_image = np.asanyarray(color_frame.get_data())

    

    roi=cv2.selectROI(windowName="roi",img=color_image,showCrosshair=True,fromCenter=False)
    cv2.destroyWindow("roi")
    x,y,w,h=roi
    widthCM = w
    heightCM = h

   

    while True:
 
        
        frames = pipeline.wait_for_frames()
        frames = align.process(frames)
        
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()
        if not depth_frame or not color_frame:
            continue
        


       
        depth_image = np.asanyarray(depth_frame.get_data())
        
        color_image = np.asanyarray(color_frame.get_data())
        
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)


        img_gray = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
        img_gb = cv2.GaussianBlur(img_gray, (5, 5), 0)
        edges = cv2.Canny(img_gray, 150 , 300)

        contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #img_fc, 
        hierarchy = hierarchy[0]
        found = []
        for i in range(len(contours)):
           k = i
           c = 0
           while hierarchy[k][2] != -1:
               k = hierarchy[k][2]
               c = c + 1
           if c >= 5:
               found.append(i)
        qq = []
        mx = []
        my = []
        for i in found:
           #img_dc = img.copy()
           M = cv2.moments(contours[i])
           cX = int(M["m10"] / M["m00"])
           cY = int(M["m01"] / M["m00"])
           qq.append([cv2.contourArea(contours[i]),cX,cY])
           #cv2.circle(color_image, (cX, cY), 7, (255, 255, 255), -1)
           cv2.drawContours(color_image, contours, i, (0, 255, 0), 3)

           zt = depth_frame.get_distance(int(cX), int(cY))
           xt = int(cX*10/widthCM)
           yt = int(cY*10/heightCM)
           text = f'x:{round(xt)} y:{round(yt)} z:{round(zt*100)}'
           cv2.putText(color_image, text, (int(cX), int(cY)), cv2.FONT_HERSHEY_COMPLEX_SMALL,1, (0, 255, 255), 1, cv2.LINE_AA)
          
        qq.sort(key=lambda k: [k[0]])

        #print(qq)
        if len(qq) ==3 or len(qq) ==4 and False:
            #print(qq[0][1])
            cv2.arrowedLine(color_image, (int(qq[2][1]),int(qq[2][2])),  (int(qq[0][1]),int(qq[0][2])), (0,0,255), 5)
            zt = depth_frame.get_distance(int(qq[2][1]), int(qq[2][2])) #*640/1280 + 50 *480/720 + 50
            cv2.circle(color_image,(int(qq[2][1]),int(qq[2][2])), 10, (0, 0, 255), -1)

            xt = int(qq[2][1]*10/widthCM)
            yt = int(qq[2][2]*10/heightCM)
            text = f'x:{round(xt)} y:{round(yt)} z:{round(zt*100)} '  #x : {cs[0]}\n y: {cs[1]}\n
            #{int(qq[2][1]*640/1280)} {int(qq[2][2]*480/720)}
            cv2.putText(color_image, text, (int(qq[2][1]), int(qq[2][2])), cv2.FONT_HERSHEY_COMPLEX_SMALL,1, (0, 255, 255), 1, cv2.LINE_AA)

      
        cv2.imshow('RealSense', color_image)
 

     
        key = cv2.waitKey(1)
        # Press esc or 'q' to close the image window
        if key & 0xFF == ord('q') or key == 27:
            cv2.destroyAllWindows()
            break
 
 
finally:
 
    # Stop streaming
    pipeline.stop()
    #plt.close(fig)
    cv2.destroyAllWindows()
    
